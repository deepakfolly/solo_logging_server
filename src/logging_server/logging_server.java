package logging_server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.CharBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.json.JSONObject;

//import testsocketserver.socketserver.SocketHandler;

public class logging_server {

private static final int CHUNKSIZE = 2048; // default 
private static final int MAX_FILE_SIZE = 600 * 1024; // 600 kb

public static void main(String[] args) throws Exception {
	ServerSocket inSocket = new ServerSocket(9444);
	System.out.println("made socket");

	File jfile = new File("/home/ubuntu/logs/open.txt");
	

	if (!jfile.exists() || !jfile.isFile()) {
		System.out.println("Could not open file");
	} else
		System.out.println("Is a file");

	while (true) {
		Socket sSocket = inSocket.accept();
		new Thread(new SocketHandler(jfile, sSocket)).start();
	}
}



static byte[] toBytes(int i) {
	byte[] result = new byte[4];

	result[0] = (byte) (i >> 24);
	result[1] = (byte) (i >> 16);
	result[2] = (byte) (i >> 8);
	result[3] = (byte) (i /* >> 0 */);

	return result;
}

private static void GetCurrentDateTime() {

    //DateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
    DateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
    //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
  //day month year hour min sec
        Date date = new Date();
        System.out.println(sdf.format(date));
        String datetimestring = sdf.format(date);
        System.out.print(datetimestring);
        
//        Calendar cal = Calendar.getInstance();
 //       System.out.println(sdf.format(cal.getTime()));

//        LocalDateTime now = LocalDateTime.now();
//        System.out.println(dtf.format(now));
//
//        LocalDate localDate = LocalDate.now();
//        System.out.println(DateTimeFormatter.ofPattern("yy/MM/dd").format(localDate));

}


private static class SocketHandler implements Runnable {

	private File jfile;
	private Socket sSocket;
	
	public SocketHandler(File jFile, Socket socket) {
		this.jfile = jFile;
		this.sSocket = socket;
	}
	
	@Override
	public void run()  {

		try {
			
			int chunkSize = CHUNKSIZE;
		
			sSocket.setSoTimeout(10000);
			
			String clientSentence = null;
			String logDeviceDetail = null;
			System.out.println("accepted socket");
			InputStreamReader 	isr = new InputStreamReader(sSocket.getInputStream());
			BufferedReader 		fromclient = new BufferedReader(isr);
			DataOutputStream 	toClient = new DataOutputStream(sSocket.getOutputStream());
			BufferedWriter bufferwriter = null;
			FileWriter filewriter = null;
			
			clientSentence = fromclient.readLine(); //{"type":"GETUPD"} STEP 1[C2S]
			System.out.println(clientSentence);
			logDeviceDetail =  clientSentence;
			
			// lets the download size 
		  	JSONObject json = new JSONObject(clientSentence.substring(2));
	        String PACKETSIZE = json.getString("PACKETSIZE");
	        int intPacketSize = Integer.parseInt(PACKETSIZE);
	        System.out.println("PACKETSIZE " + PACKETSIZE + " the converted int = " + intPacketSize);
	        chunkSize = intPacketSize;
			
	        // lets the serial number of the device
	        String serial_no = json.getString("devId");
	        LocalDate localDate = LocalDate.now();
	        LocalTime localtime = LocalTime.now();
	        System.out.println(DateTimeFormatter.ofPattern("yyyyMMdd").format(localDate));
	        System.out.println(DateTimeFormatter.ofPattern("HHmmss").format(localtime));
	        String mydate = DateTimeFormatter.ofPattern("yyyyMMdd").format(localDate);
	        String mytime = DateTimeFormatter.ofPattern("HHmmss").format(localtime);
	        final String FILENAME = "/home/ubuntu/logs/" + serial_no + "." + mydate + "." + mytime + ".log" ;
	        //("/home/ubuntu/logs");
	        filewriter  = new FileWriter(FILENAME);
	        bufferwriter = new BufferedWriter(filewriter);
	        
			/*  step 2 [S2C] */
			toClient.writeBytes("ACK"); // length of the length of payload
			toClient.flush();
			int incremental_size=0;
			char[] buff = new char[1024*5];
			while(true)
			{	
							    
				int bytesRead = fromclient.read(buff); 
				System.out.println("I received the following after sending an ACK");
				System.out.println(bytesRead);
				System.out.println(new String(buff, 0, bytesRead));
				bufferwriter.write(new String(buff, 0, bytesRead));
				incremental_size += bytesRead;
				toClient.writeBytes("ACK");
				if (incremental_size >= intPacketSize) {
					System.out.println("We have read the complete filesize ");
					break;
					
				}
			}
			try {

				if (bufferwriter != null)
					bufferwriter.close();

				if (filewriter != null)
					filewriter.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
			
		toClient.writeBytes("ACK"); // final ack
		//System.out.println("exiting loop");
		System.out.println("Logging Complete for" + logDeviceDetail);

	
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}
	
}}




